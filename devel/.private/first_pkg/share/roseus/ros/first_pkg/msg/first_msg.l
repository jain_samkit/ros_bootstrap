;; Auto-generated. Do not edit!


(when (boundp 'first_pkg::first_msg)
  (if (not (find-package "FIRST_PKG"))
    (make-package "FIRST_PKG"))
  (shadow 'first_msg (find-package "FIRST_PKG")))
(unless (find-package "FIRST_PKG::FIRST_MSG")
  (make-package "FIRST_PKG::FIRST_MSG"))

(in-package "ROS")
;;//! \htmlinclude first_msg.msg.html


(defclass first_pkg::first_msg
  :super ros::object
  :slots (_data ))

(defmethod first_pkg::first_msg
  (:init
   (&key
    ((:data __data) "")
    )
   (send-super :init)
   (setq _data (string __data))
   self)
  (:data
   (&optional __data)
   (if __data (setq _data __data)) _data)
  (:serialization-length
   ()
   (+
    ;; string _data
    4 (length _data)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _data
       (write-long (length _data) s) (princ _data s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _data
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _data (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get first_pkg::first_msg :md5sum-) "992ce8a1687cec8c8bd883ec73ca41d1")
(setf (get first_pkg::first_msg :datatype-) "first_pkg/first_msg")
(setf (get first_pkg::first_msg :definition-)
      "string data

")



(provide :first_pkg/first_msg "992ce8a1687cec8c8bd883ec73ca41d1")


