
(cl:in-package :asdf)

(defsystem "first_pkg-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "first_msg" :depends-on ("_package_first_msg"))
    (:file "_package_first_msg" :depends-on ("_package"))
  ))