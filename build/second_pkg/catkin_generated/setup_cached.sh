#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/darth/Desktop/samkit/devel/.private/second_pkg:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/darth/Desktop/samkit/devel/.private/second_pkg/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/darth/Desktop/samkit/devel/.private/second_pkg/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/darth/Desktop/samkit/build/second_pkg"
export ROSLISP_PACKAGE_DIRECTORIES="/home/darth/Desktop/samkit/devel/.private/second_pkg/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/darth/Desktop/samkit/src/second_pkg:$ROS_PACKAGE_PATH"