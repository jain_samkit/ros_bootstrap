#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/darth/Desktop/samkit/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/flyt/flytos/flytcore/lib:/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/usr/local/cuda-9.1/lib64:/home/darth/torch/install/lib:/usr/lib/nvidia-396"
export PATH="/flyt/flytos/flytcore/bin:/opt/ros/kinetic/bin:/usr/local/cuda-9.1/bin:/home/darth/torch/install/bin:/home/darth/bin:/home/darth/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PWD="/home/darth/Desktop/samkit/build/catkin_tools_prebuild"
export ROSLISP_PACKAGE_DIRECTORIES="/home/darth/Desktop/samkit/devel/.private/catkin_tools_prebuild/share/common-lisp"
export ROS_PACKAGE_PATH="/home/darth/Desktop/samkit/build/catkin_tools_prebuild:$ROS_PACKAGE_PATH"